package com.example.imcgym.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;
@Entity(tableName = "evaluation")
public class EvaluationEntity implements IEvaluation {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "date")
    private Date dayDate;

    @ColumnInfo(name = "imc")
    private Double imc;




    public EvaluationEntity(long id, Date dayDate, Double imc) {
        this.id = id;
        this.dayDate = dayDate;
        this.imc = imc;

    }


    @Override
    public long getId() {
        return id;
    }

    @Override
    public java.util.Date getdayDate() {
        return dayDate;
    }

    @Override
    public Double getImc() {
        return imc;
    }
}
