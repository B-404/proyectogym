package com.example.imcgym.models;

import java.util.Date;

public interface IUser {
     long getId();
     String getUsername();
     String getFirstname();
    String getLastname();
    Date getBirthday();
    Double getHeight();
    String getPassword();
}
