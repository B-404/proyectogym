package com.example.imcgym.models;

public class UserMapper {

    private IUser user;

    public UserMapper(IUser user) {
        this.user = user;
    }

    public User toBase(){
        User user = new User (
                this.user.getUsername(),
                this.user.getFirstname(),
                this.user.getLastname(),
                this.user.getBirthday(),
                this.user.getHeight()
        );
        user.setId(this.user.getId());
        user.setPassword(this.user.getPassword());
        return user;
    }
    public UserEntity toEntity(){
        return new UserEntity(
                this.user.getId(),
                this.user.getUsername(),
                this.user.getFirstname(),
                this.user.getLastname(),
                this.user.getBirthday(),
                this.user.getHeight(),
                this.user.getPassword()
        );
    }
}
