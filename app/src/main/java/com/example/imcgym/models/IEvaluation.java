package com.example.imcgym.models;

import java.util.Date;

public interface IEvaluation {
     long getId();
     Date getdayDate();
     Double getImc();

}
