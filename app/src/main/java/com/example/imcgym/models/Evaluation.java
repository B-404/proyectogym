package com.example.imcgym.models;

import java.io.Serializable;
import java.util.Date;

public class Evaluation implements Serializable,IEvaluation {
    private long id;
    private Date dayDate;
    private Double Imc;

    public Evaluation( Date dayDate, Double imc) {
        this.dayDate = dayDate;
        Imc = imc;
    }

    public long getId() {
        return id;
    }

    @Override
    public Date getdayDate() {
        return null;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDayDate() {
        return dayDate;
    }

    public void setDayDate(Date dayDate) {
        this.dayDate = dayDate;
    }

    public Double getImc() {
        return Imc;
    }

    public void setImc(Double imc) {
        Imc = 1.70*1.70;
    }
}
