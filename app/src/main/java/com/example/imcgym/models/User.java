package com.example.imcgym.models;

import java.util.Date;

public class User implements IUser {
     private long id;
     private String username;
     private String firstname;
     private String lastname;
     private Date birthday;
     private Double height;
     private String password;

     public User(String username, String firstname, String lastname, Date birthday, Double height) {
          this.username = username;
          this.firstname = firstname;
          this.lastname = lastname;
          this.birthday = birthday;
          this.height = height;
     }


     public long getId() {
          return id;
     }

     public void setId(long id) {
          this.id = id;
     }

     public String getUsername() {
          return username;
     }

     public String getFirstname() {
          return firstname;
     }

     public String getLastname() {
          return lastname;
     }

     public Date getBirthday() {
          return birthday;
     }

     public Double getHeight() {
          return height;
     }

     public String getPassword() {
          return password;
     }

     public void setPassword(String password) {
          this.password = password;
     }

     public String getHeightString(){
          return Double.toString(height);
     }

}