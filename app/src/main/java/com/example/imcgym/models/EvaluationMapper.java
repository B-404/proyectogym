package com.example.imcgym.models;

public class EvaluationMapper {
    private IEvaluation evaluation;

    public EvaluationMapper(IEvaluation evaluation){
        this.evaluation = evaluation;
    }

    public Evaluation toBase(){
        Evaluation base = new Evaluation(
                this.evaluation.getdayDate(),
                this.evaluation.getImc()
        );
        base.setId(this.evaluation.getId());
        return base;
    }

    public EvaluationEntity toEntity(){
        return new EvaluationEntity(
                this.evaluation.getId(),
                this.evaluation.getdayDate(),
                this.evaluation.getImc()
        );
    }
}
