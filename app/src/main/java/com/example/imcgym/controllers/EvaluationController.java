package com.example.imcgym.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.example.imcgym.dao.EvaluationDao;
import com.example.imcgym.dao.UserDao;
import com.example.imcgym.lib.GymAppDatabase;
import com.example.imcgym.models.Evaluation;
import com.example.imcgym.models.EvaluationEntity;
import com.example.imcgym.models.EvaluationMapper;
import com.example.imcgym.models.User;
import com.example.imcgym.models.UserEntity;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EvaluationController {
    private Context ctx;
    private EvaluationDao evaluationDao;

    public EvaluationController(Context ctx, EvaluationDao evaluationDao) {
        this.ctx = ctx;
        this.evaluationDao = evaluationDao;
    }

    public EvaluationController(Context ctx) {
        this.ctx = ctx;
        this.evaluationDao = GymAppDatabase.getInstance(ctx).evaluationDao();
    }

    public void register(Evaluation evaluation) {
        EvaluationMapper mapper = new EvaluationMapper(evaluation);
        EvaluationEntity newEv = mapper.toEntity();
        evaluationDao.insert(newEv);
        ((Activity) ctx).onBackPressed();
    }

    public void delete(long id) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    try {
                        evaluationDao.delete(id);
                        ((Activity) ctx).onBackPressed();
                    } catch (Error error) {
                        error.printStackTrace();
                        Toast.makeText(this.ctx, "Error al eliminar la tarea", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this.ctx);
        builder.setMessage("Estás seguro de eliminar la Evaluacion?")
                .setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();
    }

    public List<Evaluation> getAll() {
        AuthController authController = new AuthController(ctx);
        User user = authController.getUserSession();
        List<EvaluationEntity> EntityList = evaluationDao.findAll(user.getId());
        List<Evaluation> evaluationList = new ArrayList<>();

        for (EvaluationEntity evaluationEntity : EntityList) {
            EvaluationMapper mapper = new EvaluationMapper(evaluationEntity);
            Evaluation evaluation = mapper.toBase();
            evaluationList.add(evaluation);
        }
        return evaluationList;
    }

    public List<Evaluation> getRange(Date from, Date to) {
        AuthController authController = new AuthController(ctx);
        User user = authController.getUserSession();
        List<EvaluationEntity> EntityList = evaluationDao.findByRange(from, to, user.getId());
        List<Evaluation> evaList = new ArrayList<>();

        for(EvaluationEntity evaluationEntity : EntityList) {
            EvaluationMapper mapper = new EvaluationMapper(evaluationEntity);
            Evaluation evaluation = mapper.toBase();
            evaList.add(evaluation);
        }

        return evaList;
    }
}
