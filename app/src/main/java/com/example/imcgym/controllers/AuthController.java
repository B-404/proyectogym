package com.example.imcgym.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.example.imcgym.LoginActivity;
import com.example.imcgym.MainActivity;
import com.example.imcgym.RegisterActivity;
import com.example.imcgym.dao.UserDao;
import com.example.imcgym.lib.BCrypt;
import com.example.imcgym.lib.GymAppDatabase;
import com.example.imcgym.models.User;
import com.example.imcgym.models.UserEntity;
import com.example.imcgym.models.UserMapper;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;

public class AuthController {
    private final String KEY_USER_ID = "userId";
    private final String KEY_USER_USERNAME = "userName";
    private final String KEY_USER_FIRSTNAME = "userFirstname";
    private final String KEY_USER_LASTNAME = "userLastname";
    private final String KEY_USER_HEIGHT = "userHeight";

    private UserDao userDao;
    private Context ctx;
    private SharedPreferences preferences;


    public AuthController(Context ctx) {
        this.ctx = ctx;
        int PRIVATE_MODE = 0;
        String PREF_NAME ="GymAppPref";
        this.preferences = ctx.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        this.userDao = GymAppDatabase.getInstance(ctx).userDao();
    }

    private void setUserSession(User user){
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putLong(KEY_USER_ID,user.getId());
        editor.putString(KEY_USER_USERNAME, user.getUsername());
        editor.putString(KEY_USER_FIRSTNAME, user.getFirstname());
        editor.putString(KEY_USER_LASTNAME, user.getLastname());
        editor.putString(KEY_USER_HEIGHT,user.getHeightString());
        editor.apply();
    }

    public User getUserSession(){
        long id = preferences.getLong(KEY_USER_ID,0);
        String username = preferences.getString(KEY_USER_USERNAME,"");
        String firstName = preferences.getString(KEY_USER_FIRSTNAME,"");
        String lastName = preferences.getString(KEY_USER_LASTNAME,"");

        User user = new User(username,firstName,lastName,new Date(),1.70);
        user.setId(id);
        return  user;
    }

    public void checkUserSession(){
        long id = preferences.getLong(KEY_USER_ID,0);
        if(id !=0){
            Intent i = new Intent(ctx,MainActivity.class);
            ctx.startActivity(i);
            ((Activity)ctx).finish();
        }
    }

    public void register (User user){
        String hashedPassword = BCrypt.hashpw(user.getPassword(),BCrypt.gensalt());
        Log.d("Password",String.format("PW: $s / Hash: %s",user.getPassword(),hashedPassword));
        user.setPassword(hashedPassword);

        UserEntity userEntity = new UserMapper(user).toEntity();

        userDao.insert(userEntity);

        Toast.makeText(ctx,String.format("Usuario %s Registrado",user.getUsername()),Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ctx, LoginActivity.class);
        ctx.startActivity(i);
    }

    public void login(String username,String password){
        UserEntity userEntity = userDao.findByUser(username);
        
        if(userEntity == null){
            Toast.makeText(ctx, "Credenciales Invalidas", Toast.LENGTH_SHORT).show();
            return;
        }
        User user = new UserMapper(userEntity).toBase();

        if(BCrypt.checkpw(password,user.getPassword())){
            setUserSession(user);
            Toast.makeText(ctx,String.format("Bienvenido %s",username) , Toast.LENGTH_SHORT).show();
            Intent i = new Intent(ctx, MainActivity.class);
            ctx.startActivity(i);
            ((Activity)ctx).finish();
        }else{
            Toast.makeText(ctx, "Credenciales Invalidas", Toast.LENGTH_SHORT).show();
        }
    }

    public void logout(){
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.clear();
        editor.apply();
        Toast.makeText(ctx, "Cerrando Sesion", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ctx,LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);
        ((Activity)ctx).finish();

    }
}
