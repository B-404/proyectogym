package com.example.imcgym.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.imcgym.R;
import com.example.imcgym.models.Evaluation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EvaluationAdapter extends BaseAdapter {
    private Context ctx;
    private List<Evaluation> listEva;

    public EvaluationAdapter(Context ctx, List<Evaluation> listEva) {
        this.ctx = ctx;
        this.listEva = listEva;
    }

    @Override
    public int getCount() {
        return listEva.size();
    }

    @Override
    public Object getItem(int i) {
        return listEva.get(i);
    }

    @Override
    public long getItemId(int i) {
        return listEva.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(ctx);

        view = inflater.inflate(R.layout.item_evaluation,null);

        Evaluation evaluation = listEva.get(i);

        TextView tvid = view.findViewById(R.id.item_evaluation_tv_id);
         TextView tvHeight = view.findViewById(R.id.item_evaluation_tv_height);






        return view;
    }
}
