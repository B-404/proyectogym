package com.example.imcgym.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.imcgym.models.UserEntity;

@Dao
public interface UserDao {
    @Query("SELECT id,user,first_name,last_name,birthday,height,password FROM user WHERE user = :username LIMIT 1")
    UserEntity findByUser (String username);

    @Insert
    long insert(UserEntity user);
}
