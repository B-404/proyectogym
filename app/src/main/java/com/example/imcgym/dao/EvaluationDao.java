package com.example.imcgym.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.imcgym.models.EvaluationEntity;
import com.example.imcgym.models.UserEntity;

import java.util.Date;
import java.util.List;

@Dao
public interface EvaluationDao {
    @Query("SELECT id,birthday,height FROM user WHERE user = :userId LIMIT 1")
    List<EvaluationEntity> findAll(long userId);


    @Query("SELECT id, birthday FROM user WHERE user  = :userId  BETWEEN :from AND :to")
    List<EvaluationEntity> findByRange (Date from, Date to, long userId);


    @Insert
    long insert(EvaluationEntity evaluationEntity);


    @Query("DELETE FROM user WHERE id = :id")
    void delete(long id);


}
